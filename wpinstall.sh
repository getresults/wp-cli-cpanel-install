#!/bin/bash -e

## Original idea credit:  http://www.ltconsulting.co.uk/automated-wordpress-installation-with-bash-wp-cli/


dbname='happyd_foo'
dbuser='happyd_foo'
dbpass="foobar"

sitename="Happy Zine"
siteurl="http://happyd.enspiraldev.com"
sitepass="test"

adminuser="phill"
adminpass="test"
adminemail="phillcoxon@gmail.com"


clear

echo "================================================================="
echo "WordPress WP-CLI Cpanel Installer"
echo "================================================================="

echo ""
echo ""
echo "-----------------------------------------------------------------"
echo "Database Info"
echo "-----------------------------------------------------------------"
echo ""
echo ""

# accept user input for the databse name
echo "Database Name: (cpuser_dbname)"
read -e dbname

# accept user input for the databse name
echo "Database User: (cpuser_dbuser)"
read -e dbuser

# accept user input for the databse name
echo "Database Password: "
read -e dbpass

echo ""
echo "-----------------------------------------------------------------"
echo "Site Info"
echo "-----------------------------------------------------------------"
echo ""
echo ""


# accept the name of our website
echo "Site Title: "
read -e sitename

# accept the name of our website
echo "Site URL: "
read -e siteurl

# accept the name of our website
echo "Admin User: "
read -e adminuser

# accept the name of our website
echo "Admin Password: "
read -e adminpass

# accept the name of our website
echo "Admin Email: "
read -e adminemail

# add a simple yes/no confirmation before we proceed
echo "Run Install? (y/n)"
read -e run

# if the user didn't say no, then go ahead an install
if [ "$run" == n ] ; then
exit
else

# download the WordPress core files
wp core download

# create the wp-config file
wp core config --dbname=$dbname --dbuser=$dbuser --dbpass=$dbpass

# parse the current directory name
currentdirectory=${PWD##*/}

# generate random 12 character password
# password=$(LC_CTYPE=C tr -dc A-Za-z0-9_\!\@\#\$\%\^\&\*\(\)-+= < /dev/urandom | head -c 12)

# create database, and install WordPress
wp db create
#wp core install --url="http://localhost/$currentdirectory" --title="$sitename" --admin_user="$wpuser" --admin_password="$password" --admin_email="user@example.org"

wp core install --url="$siteurl" --title="$sitename" --admin_user="$adminuser" --admin_password="$adminpass" --admin_email="$adminemail"


# http://forums.cpanel.net/f354/command-line-created-db-not-appearing-cpanel-but-phpmyadmin-213002.html



# install the _s theme
#wp theme install https://github.com/Automattic/_s/archive/master.zip --activate

# add a simple yes/no confirmation before we proceed
echo "Install Premium Plugin Set? (y/n)"
read -e premium

# if the user didn't say no, then go ahead an install
if [ "$premium" == n ] ; then
exit
else
	echo "This is where we'd install premium plugins"
fi


clear

echo "================================================================="
echo "Installation is complete. Your username/password is listed below."
echo "URL: $siteurl"
echo "Username: $adminuser"
echo "Password: $adminpass"
echo ""
echo "================================================================="

fi